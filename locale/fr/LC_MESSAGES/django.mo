��    	      d      �       �   �   �   4   �     �     �          
          &  j  6    �  A   �     �     	  	             0     L            	                                  
    Hi!<br><br>
    We've received a request to reset your password.
    If you did not make the request, please ignore this email.
    Otherwise, you can reset your password by clicking on the link below:<br>
 <strong>Success!</strong> Your password was changed. Confirm Password English French New Password RESET PASSWORD UPDATE PASSWORD Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-03-13 17:36+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
    Bonjour !<br><br>
    Nous avons reçu une demande pour réinitialiser ton mot de passe.
    Si cela ne vient pas de toi, tu peux simplement ignorer ce mail.
    Sinon, tu peux cliquer sur le lien en dessous pour
    pouvoir réinitialiser ton mot de passe.<br>
 <strong>C'est bon!</strong> Ton mot de passe a été mis à jour. Confirmer le mot de passe anglais français Nouveau Mot de pasee RÉINITIALISER MOT DE PASSE METTRE À JOUR MOT DE PASSE 