from __future__ import absolute_import

import os

from celery import Celery
from celery.schedules import crontab
from django.apps import apps

# set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "clickbye.settings.{}".format(os.environ.get('ENV', 'dev')))

from django.conf import settings  # noqa

app = Celery('clickbye')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
app.autodiscover_tasks(lambda: [n.name for n in apps.get_app_configs()])

app.conf.beat_schedule = {
    'send_email_to_old_account': {
        'task': 'clickbyer.tasks.send_email_to_old_accounts.main',
        'schedule': crontab(hour=9, minute=00),
    }
}
app.conf.timezone = 'UTC'
