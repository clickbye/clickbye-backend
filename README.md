# Clickbye - Backend

## Prerequisites

1. Python - 2.7

2. Pip (https://pip.pypa.io/en/stable/installing/)


## Installation

1. Install requirements:

    pip install -r requirements.txt


2. Run server:
    python manage.py runserver


## Deployment:
We host service on heroku so you should be added to the heroku project
Then login using `heroku login` command. Afterwards every push to heroku
will perform a deploy.

    git push heroku master
