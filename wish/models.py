from django.db import models
from clickbyer.models import FlightUser


class Wishlist(models.Model):
    user = models.ForeignKey(FlightUser, related_name='wishlist')
    city_name = models.CharField(max_length=50)
    country_name = models.CharField(max_length=50)
    skyscanner_city_id_destination = models.CharField(max_length=10)
    url_image = models.URLField()
    city_name_depart = models.CharField(max_length=50)
    country_name_depart = models.CharField(max_length=50)
    skyscanner_city_ids = models.CharField(max_length=255)
    user_skycanner_country_id = models.CharField(max_length=10)
    checkin = models.DateField()
    checkout = models.DateField()
    max_price = models.PositiveIntegerField()
    latest_total_price = models.PositiveIntegerField()
    latest_flight_price = models.PositiveIntegerField()
    latest_hotel_price = models.PositiveIntegerField()
    currency_code = models.CharField(max_length=50)
    adults = models.PositiveIntegerField()
    children = models.PositiveIntegerField()
    saved_screen = models.CharField(max_length=50)

    class Meta:
        unique_together = [
            "user",
            "skyscanner_city_id_destination",
            "skyscanner_city_ids",
            "checkin",
            "checkout",
            "adults",
            "children",
            "saved_screen",
        ]

    def __str__(self):
        return "{0} {1}".format(self.user.email, self.city_name)
