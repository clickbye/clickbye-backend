# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.utils.request_validators import check_param_existing, check_data_by_validator
from wish.models import Wishlist
from wish.serializers import WishlistGetSerializer
from wish.serializers import WishlistSerializer

SORT_BY = ['Soonest', 'Cheapest', 'Longest']


class WishList(APIView):
    permission_classes = (IsAuthenticated,)

    @check_param_existing('wishlist_item_id')
    def delete(self, request):
        wishlist_items = Wishlist.objects.filter(user=request.user, pk=request.POST['wishlist_item_id'])
        if wishlist_items:
            wishlist_items.delete()
            return Response({
                'Wishlist item removed': {
                    'reason': u'n/a',
                    'result': u'success'
                }}, status=200)

        return Response({
            'Item was not found': {
                'reason': u'Not found',
                'result': u'error'
            }}, status=404)

    @check_param_existing('city_name')
    @check_param_existing('country_name')
    @check_param_existing('skyscanner_city_id_destination')
    @check_param_existing('url_image')
    @check_param_existing('city_name_depart')
    @check_param_existing('country_name_depart')
    @check_param_existing('skyscanner_city_ids')
    @check_param_existing('user_skycanner_country_id')
    @check_param_existing('checkin')
    @check_param_existing('checkout')
    @check_param_existing('max_price')
    @check_param_existing('latest_total_price')
    @check_param_existing('latest_flight_price')
    @check_param_existing('latest_hotel_price')
    @check_param_existing('currency_code')
    @check_param_existing('adults')
    @check_param_existing('children')
    @check_param_existing('saved_screen')
    def post(self, request):
        if request.POST.get('user_skycanner_country_id') == request.POST.get('skyscanner_city_id_destination'):
            return Response({
                'result': 'error',
                'reason': 'zero_saved_destination'
            }, status=400)

        wishlist = WishlistSerializer(data=request.data, context={'request': request})

        if wishlist.is_valid():
            wishlist.save()
        else:
            return Response({
                'result': 'error',
                'reason': wishlist.errors
            }, status=400)

        return Response({
            'Add item to the Wish List': {
                'reason': u'n/a',
                'result': u'success'
            }})

    @check_param_existing('currency_code', method='GET')
    @check_data_by_validator('sort_by', lambda x: x in SORT_BY, method='GET')
    def get(self, request):
        wishlist = Wishlist.objects.filter(user=request.user)

        if not wishlist:
            return Response({
                'result': 'error',
                'reason': 'zero_saved_destination'
            })

        sort_by = request.GET.get('sort_by')
        if sort_by == 'Soonest':
            wishlist = wishlist.order_by('checkin')

        elif sort_by == 'Cheapest':
            wishlist = wishlist.order_by(
                'max_price',
                'latest_total_price',
                'latest_flight_price',
                'latest_hotel_price'
            )

        elif sort_by == 'Longest':
            wishlist = wishlist.order_by('-checkin')

        try:
            wishlist = list(WishlistGetSerializer(
                instance=wishlist,
                many=True,
                context={
                    'currency': request.GET.get('currency_code'),
                    'locale': request.GET.get('language_code')
                }).data)
        except (KeyError, ValueError) as error:
            return Response({
                'result': 'error',
                'reason': str(error)
            })

        return Response({
            'Wishlist': wishlist,
            'reason': u'n/a',
            'result': u'success'
        })
