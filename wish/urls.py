from django.conf.urls import url

from .views.wish_list import WishList

app_name = 'wish'
urlpatterns = [
    url(r'', WishList.as_view(), name='wish'),
]
