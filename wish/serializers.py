import math
from currency_converter import CurrencyConverter

from rest_framework import serializers
from rest_framework.fields import empty

from .models import Wishlist
from clickbyer.models import FlightUser


class WishlistSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(
        write_only=True,
        default=serializers.CurrentUserDefault(),
        queryset=FlightUser.objects.all()
    )

    class Meta:
        model = Wishlist
        fields = '__all__'


class WishlistGetSerializer(WishlistSerializer):
    max_price = serializers.SerializerMethodField()
    latest_total_price = serializers.SerializerMethodField()
    latest_flight_price = serializers.SerializerMethodField()
    latest_hotel_price = serializers.SerializerMethodField()
    currency_code = serializers.SerializerMethodField()

    def __init__(self, instance=None, data=empty, **kwargs):
        self.converter = CurrencyConverter()
        super(WishlistGetSerializer, self).__init__(instance, data, **kwargs)

    def get_max_price(self, obj):
        if 'currency' not in self.context:
            return self.initial_data['max_price']
        value = self.converter.convert(obj.max_price, obj.currency_code, self.context['currency'])
        return math.floor(value)

    def get_latest_total_price(self, obj):
        if 'currency' not in self.context:
            return self.initial_data['latest_total_price']
        value = self.converter.convert(obj.latest_total_price, obj.currency_code, self.context['currency'])
        return math.floor(value)

    def get_latest_flight_price(self, obj):
        if 'currency' not in self.context:
            return self.initial_data['latest_flight_price']
        value = self.converter.convert(obj.latest_flight_price, obj.currency_code, self.context['currency'])
        return math.floor(value)

    def get_latest_hotel_price(self, obj):
        if 'currency' not in self.context:
            return self.initial_data['latest_hotel_price']
        value = self.converter.convert(obj.latest_hotel_price, obj.currency_code, self.context['currency'])
        return math.floor(value)

    def get_currency_code(self, obj):
        if 'currency' not in self.context:
            return self.initial_data['currency_code']
        return self.context['currency']
