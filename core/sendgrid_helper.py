import sendgrid
from sendgrid.helpers.mail import Email, Content, Substitution, Mail
from django.conf import settings

# Old emails campaign
OLD_EMAILS_ENG_TEMPLATE = '1234a1e2-1da1-47a6-9245-e1311e993558'
OLD_EMAILS_FR_TEMPLATE = '467f7042-255e-4a9c-b258-dd52053e4ebd'


# Welcome campaign
WELCOME_CAMPAIGN_FIRST_EMAIL_FR = '47a6fb1f-c7c3-4920-8af2-8a228acba39b'
WELCOME_CAMPAIGN_FIRST_EMAIL_ENG = '8976c1d3-4f83-428c-98bb-64b7e7b8e967'

WELCOME_CAMPAIGN_AFTER_3_DAYS_EMAIL_FR = '5d40d7e5-f1dc-4560-af65-58e3b1cd32d3'
WELCOME_CAMPAIGN_AFTER_3_DAYS_EMAIL_ENG = '9bda9911-93ff-4ac5-84ba-6547fc85ec9b'

WELCOME_CAMPAIGN_AFTER_7_DAYS_EMAIL_FR = 'd631ee80-b5ae-4364-846a-f39d2fc05d4c'
WELCOME_CAMPAIGN_AFTER_7_DAYS_EMAIL_ENG = '8c94353a-186c-4f99-b13b-c3cf041afe02'

# Reset password
RESET_PASSWORD_ENG_TEMPLATE = '23d50c93-ae73-444e-a782-4c2d5a8ec393'
RESET_PASSWORD_FR_TEMPLATE = 'a3298b63-0035-441b-9896-e762b09f4046'

# City city selected event
CITY_CITY_SELECTED_ENG_TEMPLATE = 'd74e6b24-c38f-4133-bb84-0fe3df4c3a2c'
CITY_CITY_SELECTED_FR_TEMPLATE = 'd74e6b24-c38f-4133-bb84-0fe3df4c3a2c'


class Sendgrid:
    def __init__(self):
        self.sg = sendgrid.SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)
        self.from_email = settings.SENDGRID_FROM_EMAIL
        self.from_email_name = settings.SENDGRID_FROM_EMAIL_NAME

    def send_email_for_city_city_selected_event(self, to_email, data, is_eng=True):
        template_id = CITY_CITY_SELECTED_ENG_TEMPLATE if is_eng else CITY_CITY_SELECTED_FR_TEMPLATE
        mail = self._create_email_object_for_template(to_email, template_id, data)
        return self._send_email(mail)


    def send_email_for_resetting_password(self, to_email, link, is_eng=True):
        template_id = RESET_PASSWORD_ENG_TEMPLATE if is_eng else RESET_PASSWORD_FR_TEMPLATE
        mail = self._create_email_object_for_template(to_email, template_id, {'-link-': link})
        return self._send_email(mail)

    def send_email_for_old_emails_activity_campaign(self, to_email, name, is_eng=True):
        template_id = OLD_EMAILS_ENG_TEMPLATE if is_eng else OLD_EMAILS_FR_TEMPLATE
        mail = self._create_email_object_for_template(to_email, template_id, {'-name-': name})
        return self._send_email(mail)

    def send_email_for_welcome_campaign(self, clickbyer_email, name, is_eng):
        template_id = WELCOME_CAMPAIGN_FIRST_EMAIL_ENG if is_eng else WELCOME_CAMPAIGN_FIRST_EMAIL_FR
        mail = self._create_email_object_for_template(clickbyer_email, template_id, {'-name-': name})
        return self._send_email(mail)

    def send_email_for_welcome_campaign_after_3_days(self, clickbyer_email, name, is_eng):
        template_id = WELCOME_CAMPAIGN_AFTER_3_DAYS_EMAIL_ENG if is_eng else WELCOME_CAMPAIGN_AFTER_3_DAYS_EMAIL_FR
        mail = self._create_email_object_for_template(
            clickbyer_email,
            template_id,
            {'-name-': name}
        )
        return self._send_email(mail)

    def send_email_for_welcome_campaign_after_7_days(self, clickbyer_email, name, is_eng):
        template_id = WELCOME_CAMPAIGN_AFTER_7_DAYS_EMAIL_ENG if is_eng else WELCOME_CAMPAIGN_AFTER_7_DAYS_EMAIL_FR
        mail = self._create_email_object_for_template(
            clickbyer_email,
            template_id,
            {'-name-': name}
        )
        return self._send_email(mail)

    def _send_email(self, mail):
        return self.sg.client.mail.send.post(request_body=mail.get())

    def _create_email_object_for_template(self, to_email, template_id, substitutions=None):
        mail = Mail(
            from_email=Email(self.from_email, name=self.from_email_name),
            subject='empty',  # requires by the API - template has its own subject
            to_email=Email(to_email),
            content=Content("text/html", "empty")  # requires by the API - template has its own content
        )
        mail.template_id = template_id

        if substitutions:
            for k, v in substitutions.items():
                mail.personalizations[0].add_substitution(Substitution(k, v))
        return mail
