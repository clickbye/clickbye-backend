# -*- coding: utf-8 -*-

from django.contrib import admin
from api.models.airport import Airport
from api.models.reset_code import ResetCode
from api.models.sky_scanner_cache import SkyScannerFlightsCache
from api.models.sky_scanner_city import CityData


admin.site.register(Airport)
admin.site.register(ResetCode)
admin.site.register(SkyScannerFlightsCache)
admin.site.register(CityData)
