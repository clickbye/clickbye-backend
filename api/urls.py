from django.conf.urls import url

from api.views.get_closest_airports import GetClosestAirports
from api.views.get_closest_city import GetClosestCity
from api.views.get_destinations import GetDestinations
from api.views.get_flights import GetFlights
from api.views.get_hotel_detials import GetHotelsDetails
from api.views.get_most_expensive_hotels import GetMostExpensiveHotels

app_name = 'api'
urlpatterns = [
    url(r'^search/destinations/$', GetDestinations.as_view(), name='get_destinations'),
    url(r'^search/flights/$', GetFlights.as_view(), name='get_flights'),

    url(r'^search/hotels/$', GetHotelsDetails.as_view(), name='get_hotels_details'),
    url(r'^search/most_expensive_hotels_in_cities/$', GetMostExpensiveHotels.as_view(),
        name='get_most_expensive_hotels_in_cities'),

    url(r'^search/closest_airports/$', GetClosestAirports.as_view(), name='Get_closest_airports'),
    url(r'^search/closest_city/$', GetClosestCity.as_view(), name='Get_closest_city'),

    # OLD API TO REMOVE
    url(r'^get_destinations$', GetDestinations.as_view(), name='get_destinations_OLD'),
    url(r'^get_flights$', GetFlights.as_view(), name='get_flights_OLD'),

    url(r'^get_hotels_details$', GetHotelsDetails.as_view(), name='get_hotels_details_OLD'),
    url(r'^get_most_expensive_hotels_in_cities$', GetMostExpensiveHotels.as_view(),
        name='get_most_expensive_hotels_in_cities_OLD'),

    url(r'^get_closest_airports$', GetClosestAirports.as_view(), name='Get_closest_airports_OLD'),
    url(r'^get_closest_city$', GetClosestCity.as_view(), name='Get_closest_city_OLD'),
]
