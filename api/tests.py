# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import time

import json
import pprint
import requests


# Print results in views.py
def printReturnValues(json_returned):
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(json_returned)


def getTimestampfromDate(day):
    dt = datetime.datetime.strptime(day, '%Y-%m-%d')
    return int(time.mktime(dt.timetuple()))


# Create your tests here.
if __name__ == '__main__':
    # https://darksky.net/dev/docs/time-machine
    timestamp = getTimestampfromDate('2017-10-03')
    url = "https://api.darksky.net/forecast/62aa6776608a07843f9c2a3b10d58120/32.109333,34.855499," + str(
        timestamp) + "?units=si"
    result = requests.get(url, headers={'Accept': "application/json", "Accept-Charset": "utf-8"})

    dict_json = json.loads(result.text)

    printReturnValues(dict_json.get('currently').get('temperature'))
