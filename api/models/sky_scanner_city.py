# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.postgres.fields import ArrayField


class CityData(models.Model):
    skyscanner_id = models.CharField(max_length=20, unique=True)
    country_name = models.CharField(max_length=100, null=True, blank=True)
    country_id = models.CharField(max_length=100, null=True, blank=True)
    city_name = models.CharField(max_length=100)
    wishes = ArrayField(models.CharField(max_length=20), blank=True)
    max_pics = models.IntegerField(null=True, blank=True, default=0)
    bcom_id = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return "{} - {}".format(self.city_name.encode('utf-8'), self.skyscanner_id)
