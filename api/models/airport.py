# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.postgres.fields import JSONField
from django.db import models


class Airport(models.Model):
    place_id = models.CharField(max_length=50, unique=True)
    city_id = models.CharField(max_length=50)
    name = models.CharField(max_length=200)
    country_id = models.CharField(max_length=50)
    place_name = models.CharField(max_length=100)
    country_name = models.CharField(max_length=100)
    latitude = models.CharField(max_length=20)
    longitude = models.CharField(max_length=20)

    def __str__(self):
        return self.name

    def get_json(self):
        return {
            'city_id': self.city_id,
            'place_name': self.place_name,
            'country_id': self.country_id,
            'country_name': self.country_name,
            'PlaceId': self.place_id,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'name': self.name
        }

    def create_from_json(self, data):
        self.place_id = data['PlaceId']
        self.city_id = data['city_id']
        self.name = data['name']
        self.place_name = data['place_name']
        self.country_id = data['country_id']
        self.country_name = data['country_name']
        self.longitude = data['longitude']
        self.latitude = data['latitude']
        self.save()
        return self
