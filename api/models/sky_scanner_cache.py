# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.postgres.fields import JSONField
from django.db import models


class SkyScannerFlightsCache(models.Model):
    query = models.CharField(max_length=200, db_index=True, unique=True)
    flights = JSONField(default={})
    modified = models.DateTimeField('date_created', db_index=True, auto_now=True)

    def __str__(self):
        return self.query
