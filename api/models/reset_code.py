# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import models

from api.utils import generate_code


class ResetCode(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                related_name='ps_reset_code',
                                on_delete=models.SET_NULL, null=True)

    code = models.CharField(max_length=255, default=generate_code)

    def refresh_code(self):
        """
        Refresh code after use old one
        :return: None
        """
        self.code = generate_code()
        self.save()
        return None

    def __str__(self):
        return "{0}, {1}".format(self.user.username, self.code)
