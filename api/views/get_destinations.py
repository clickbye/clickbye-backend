# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import defaultdict
from multiprocessing.dummy import Pool as ThreadPool

import requests
from django.conf import settings
from django.http import JsonResponse
from rest_framework.views import APIView

from api.models.sky_scanner_city import CityData
from api.utils import is_int, valid_date
from api.utils.hotel_price_getter import HotelPriceGetter
from api.utils.image_urls_builder import ImageUrlsBuilder
from api.utils.request_validators import check_param_existing, check_data_by_validator
from api.utils.translated_wishes_getter import TranslatedWishesGetter

list_hotels_selection = ['cheapest', 'average', 'most_expensive']
list_sort_by = ["cheapest_total", "cheapest_flight", "cheapest_hotel"]


class GetDestinations(APIView):
    @check_param_existing('skyscanner_city_ids', method='GET')
    @check_param_existing('user_skyscanner_country_id', method='GET')
    @check_data_by_validator('checkin', valid_date, method='GET')
    @check_data_by_validator('checkout', valid_date, method='GET')
    @check_param_existing('language', method='GET')
    @check_data_by_validator('max_price', is_int, method='GET')
    @check_data_by_validator('stars', is_int, method='GET')
    @check_param_existing('currency_code', method='GET')
    @check_param_existing('locale', method='GET')
    @check_data_by_validator('adults', is_int, method='GET')
    @check_data_by_validator('children', is_int, method='GET')
    @check_data_by_validator('is_flight_only', lambda x: is_int(x) and int(x) in (0, 1), method='GET')
    @check_data_by_validator('hotels_selection', lambda x: x in list_hotels_selection, method='GET')
    @check_data_by_validator('sort_by', lambda x: x in list_sort_by, method='GET')
    @check_param_existing('wishes', method='GET')
    @check_param_existing('skyscanner_destination_country_id', method='GET')
    def get(self, request):
        skyscanner_destination_country_id = request.GET['skyscanner_destination_country_id']
        is_flight_only = bool(int(request.GET['is_flight_only']))
        max_price = int(request.GET['max_price'])

        stars = int(request.GET['stars'])
        stars = stars if 1 < stars > 5 else -1

        list_wishes_to_search = self._get_wishes_to_search(request.GET['wishes'])
        list_skyscanner_city_ids = request.GET['skyscanner_city_ids'].split(',')

        list_request_several_origin_destinations = []
        for skyscanner_city_id in list_skyscanner_city_ids:
            list_request_several_origin_destinations.append([
                request,
                skyscanner_city_id,
                max_price,
                stars,
                is_flight_only,
                list_wishes_to_search,
                skyscanner_destination_country_id
            ])

        # Go through all cities
        list_results = []
        list_hotels_requests = []
        if len(list_request_several_origin_destinations) > 1:
            thread_results = []
            pool = ThreadPool(5)

            list_thread_returned = pool.map(
                self.make_get_destinations_main_query,
                list_request_several_origin_destinations
            )

            for row in list_thread_returned:
                thread_results.append(row)

            pool.close()
            pool.join()

            for row in thread_results:
                row = self.process_flights_results(*row)
                list_results.extend(row[0])
                list_hotels_requests.extend(row[1])

        else:
            places, quotes, args = self.make_get_destinations_main_query(list_request_several_origin_destinations[0])
            result = self.process_flights_results(places, quotes, args)
            list_results.extend(result[0])
            list_hotels_requests.extend(result[1])

        list_hotels_requests = self._modify_list_hotels_requests(list_hotels_requests)

        # Get Hotels Data
        if not is_flight_only:
            list_results_return = []
            pool = ThreadPool(20)
            list_hotels_prices = pool.map(self.get_hotel_price, list_hotels_requests)

            pool.close()
            pool.join()

            # Update results with Hotel Price
            for row in list_results:
                for elem in list_hotels_prices:
                    if elem[0].skyscanner_id == row.get('skyscanner_city_id'):
                        row['hotel_price'] = elem[1]

                        if row['hotel_price'] is not None:
                            row['total_price'] = row.get('flight_price') + elem[1]
                            list_results_return.append(row)
                        break

        else:
            list_results_return = list_results

        list_results_return = self._remove_duplicated_destination_city(list_results_return)

        context = {
            'result': 'success',
            'reason': 'n/a',
            'destinations': self._sort_by_price(list_results_return, request.GET['sort_by'])
        }

        return JsonResponse(context)

    def make_get_destinations_main_query(self, args):
        request, city_id, _, _, _, _, destination_country_id = args
        url = self._build_skyscanner_url(request, city_id, destination_country_id)
        response = requests.get(url, headers={'Accept': "application/json", "Accept-Charset": "utf-8"})
        content = response.json()
        places = content.get("Places", [])
        quotes = content.get('Quotes', [])
        return places, quotes, args

    def process_flights_results(self, places, quotes, args):
        request, departure_city_id, max_price, stars, is_flight_only, list_wishes_to_search, destination_country_id = args
        list_wishes_to_search = [
            e['name'].lower() for e in TranslatedWishesGetter().get_wishes_list(
                list_wishes_to_search, request.GET['language']
            )
        ]
        list_results = []
        list_hotels_requests = []
        number_of_people = int(request.GET['adults']) + int(request.GET['children'])
        quotes = self._filter_quotes_by_price(quotes, max_price, number_of_people)
        dict_places = self._get_skyscanner_dict_place(places, quotes)

        skyscanner_objects_cache = self._get_skyscanner_objects_cache(dict_places, quotes)

        list_cities_already_returned = []
        for i, quote in enumerate(quotes):
            destination_id = quote.get('OutboundLeg').get('DestinationId')
            destination = dict_places.get(destination_id)

            if destination[1] not in list_cities_already_returned:

                list_cities_already_returned.append(destination[1])

                flight_price = int(quote.get('MinPrice')) * number_of_people
                destination_city_id = destination[0] + '-sky'

                skyscanner_obj = skyscanner_objects_cache[destination_city_id]

                list_wishes, list_wishes_names = self._get_wishes_lists(
                    skyscanner_obj.wishes,
                    request.GET['language']
                )
                list_wishes_commons = set(list_wishes_to_search) & set(list_wishes_names)

                if not list_wishes_to_search or list_wishes_commons:
                    url_image = ImageUrlsBuilder().get_destination_url_picture(skyscanner_obj)

                    hotel_price = None
                    if not is_flight_only:
                        list_hotels_requests.append(
                            [request.GET['hotels_selection'], destination_city_id, (max_price - flight_price),
                             request.GET['adults'], request.GET['children'], request.GET['checkin'],
                             request.GET['checkout'], request.GET['currency_code'], stars])

                    total_price = flight_price

                    row = {
                        'departure_city_id': departure_city_id,
                        'flight_price': flight_price,
                        'skyscanner_city_id': destination_city_id,
                        'city_name': destination[1],
                        'country_name': destination[2],
                        'country_id': self._get_skyscanner_country_id_from_location_id(
                            skyscanner_obj,
                            request.GET['locale'],
                            destination_city_id
                        ),
                        'hotel_price': hotel_price,
                        'total_price': total_price,
                        'wishes': list_wishes,
                        'url_image': url_image
                    }

                    list_results.append(row)
        return [list_results, list_hotels_requests]

    def _get_skyscanner_objects_cache(self, dict_places, quotes):
        d = set()
        for q in quotes:
            destination = dict_places.get(q.get('OutboundLeg').get('DestinationId'))
            d.add("{}-sky".format(destination[0]))
        data = CityData.objects.filter(skyscanner_id__in=d)
        return dict(((obj.skyscanner_id, obj) for obj in data))

    def _modify_list_hotels_requests(self, list_hotels_requests):
        skyscanner_ids = set(e[1] for e in list_hotels_requests)

        skyscanner_data = dict((
            (i.skyscanner_id, i) for i in CityData.objects.filter(
                skyscanner_id__in=skyscanner_ids
            )
        ))

        for index in range(len(list_hotels_requests)):
            list_hotels_requests[index][1] = skyscanner_data[list_hotels_requests[index][1]]

        return list_hotels_requests

    @staticmethod
    def _filter_quotes_by_price(quotes, max_price, number_of_people):
        return [
            q for q in quotes if
            (q['MinPrice'] * number_of_people) <= max_price and 'OutboundLeg' in q and 'InboundLeg' in q
        ]

    @staticmethod
    def get_hotel_price(args):
        price = HotelPriceGetter().get_hotel_price(*args)
        return args[1], price

    @staticmethod
    def _get_wishes_to_search(wishes):
        return wishes.split(',') if wishes.lower().strip() != 'all' else []

    @staticmethod
    def _build_skyscanner_url(request, city_id, country_id):
        url = "http://partners.api.skyscanner.net/apiservices/browsequotes/v1.0/{}/{}/{}/{}/{}/{}/{}?apiKey={}"
        return url.format(
            request.GET['user_skyscanner_country_id'],
            request.GET['currency_code'],
            request.GET['locale'],
            city_id,
            country_id,
            request.GET['checkin'],
            request.GET['checkout'],
            settings.SKYSCANNER_API_KEY
        )

    @staticmethod
    def _get_skyscanner_dict_place(places, quotes):
        quotes_destinations = [quote.get('OutboundLeg').get('DestinationId') for quote in quotes]
        dict_places = {}

        for elem in places:
            if elem['PlaceId'] in quotes_destinations:
                country_name = elem.get('CountryName') or elem['Name']
                city_name = elem.get('CityName')
                city_id = elem.get('CityId') or elem['SkyscannerCode']
                dict_places[elem['PlaceId']] = [city_id, city_name, country_name]

        return dict_places

    @staticmethod
    def _get_wishes_lists(skyscanner_obj_wishes, language):
        wishes_list = TranslatedWishesGetter().get_wishes_list(skyscanner_obj_wishes, language)
        wishes_names = [wish['name'].lower() for wish in wishes_list]
        return wishes_list, wishes_names

    @staticmethod
    def _get_skyscanner_country_id_from_location_id(skyscanner_obj, locale, skyscanner_city_id):
        if skyscanner_obj.country_id:
            return skyscanner_obj.country_id

        url = "http://partners.api.skyscanner.net/apiservices/autosuggest/v1.0/UK/GBP/{}/?id={}&apiKey={}".format(
            locale, skyscanner_city_id, settings.SKYSCANNER_API_KEY
        )
        response = requests.get(url, headers={'Accept': "application/json", "Accept-Charset": "utf-8"})

        if response.ok:
            skyscanner_obj.country_id = response.json().get('Places')[0].get('CountryId')
            skyscanner_obj.save()
            return skyscanner_obj.country_id

    @staticmethod
    def _sort_by_price(results, sort_by):
        if sort_by == 'cheapest_flight':
            return sorted(results, key=lambda k: k['flight_price'])
        return sorted(results, key=lambda k: k['total_price'])

    @staticmethod
    def _remove_duplicated_destination_city(results):
        seen = defaultdict(dict)
        for elem in results:
            price = seen[elem['city_name']].get('total_price')
            if price is None:
                seen[elem['city_name']] = elem

            elif price > elem['total_price']:
                seen[elem['city_name']] = elem
        return seen.values()
