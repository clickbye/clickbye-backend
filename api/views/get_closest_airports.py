# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import requests
from multiprocessing.pool import ThreadPool
from django.conf import settings
from django.http import JsonResponse
from rest_framework.views import APIView

from api.models.airport import Airport
from api.utils import is_int
from api.utils.request_validators import check_param_existing, check_data_by_validator

list_sort_by = ['quickest']


class GetClosestAirports(APIView):
    MILES_DELTA = 0.621371

    @check_param_existing('latitude', method='GET')
    @check_param_existing('longitude', method='GET')
    @check_data_by_validator('radius', is_int, method='GET')
    @check_param_existing('market', method='GET')
    @check_param_existing('locale', method='GET')
    @check_param_existing('currency_code', method='GET')
    @check_param_existing('length_unit', method='GET')
    @check_param_existing('distance_mode', method='GET')
    @check_data_by_validator('sort_by', lambda x: x in list_sort_by, method='GET')
    def get(self, request):
        result = {'closest_airports': [], 'result': 'success', 'reason': 'n/a'}
        length_unit = request.GET.get('length_unit')

        radius = int(request.GET.get('radius'))
        radius *= self.MILES_DELTA if length_unit == 'kms' else 1
        radius = int(radius)

        response = requests.get(
            settings.FLIGHTSTATS_GET_AIRPORTS_WITHIN_RADIUS.format(
                longitude=request.GET.get('longitude'),
                latitude=request.GET.get('latitude'),
                radiusMiles=radius,
                app_id=settings.FLIGHTSTATS_AIRPORTS_APP_ID
            )
        )
        response = response.json()
        if "error" in response.keys():
            return JsonResponse(response)

        airports = [
            airport for airport in response['airports'] if
            'iata' in airport.keys() and airport['classification'] != 5
        ]

        fetched_airports = [
            airport.get_json() for airport in
            Airport.objects.filter(place_id__in=["{}-sky".format(e['iata']) for e in airports])
        ]
        created_airports = []

        if len(fetched_airports) != len(airports):
            fetched_airports_ids = [e['PlaceId'] for e in fetched_airports]
            not_fetched_airports = [airport for airport in airports if
                                    "{}-sky".format(airport['iata']) not in fetched_airports_ids]

            pool = ThreadPool(5)
            airports = pool.map(self._update_airports_details, [(request, airport) for airport in not_fetched_airports])

            pool.close()
            pool.join()

            airports = [airport for airport in airports if 'city_id' in airport.keys()]
            for airport in airports:
                created_airports.append(Airport().create_from_json(airport).get_json())

        airports = created_airports + fetched_airports
        destinations = ""
        for airport in airports:
            destinations += "{},{}|".format(airport['latitude'], airport['longitude'])
        destinations = destinations[:-1]

        response = requests.get(
            settings.GOOGLE_METRIX_API.format(
                units='metric' if length_unit == 'kms' else 'imperial',
                locale=request.GET.get('locale'),
                mode=request.GET.get('distance_mode'),
                lat=request.GET.get('latitude'),
                long=request.GET.get('longitude'),
                dest=destinations,
                app_id=settings.GOOGLE_METRIX_API_KEY
            )
        )
        if not response.ok:
            raise Exception('Google API returns not OK response')

        google_resp = response.json()
        google_rows = google_resp['rows']
        elements = google_rows[0].get('elements', {}) if google_rows else []
        for i, item in enumerate(elements):
            try:
                airport_distance = int(float(
                    ''.join(item['distance']['text'].split()[:-1]).replace(',', '.')
                ))
                distance_unit = item['distance']['text'].split()[-1]
                airport_dict = {
                    'Airport_name': airports[i]['name'],
                    'City_name': airports[i]['place_name'],
                    'Country_name': airports[i]['country_name'],
                    'Skyscanner_airport_id': airports[i]['PlaceId'],
                    'Airport_travel_time': item['duration']['text'],
                    'Airport_distance': "{} {}".format(airport_distance, distance_unit),
                    'distance_value': item['distance']['value'],
                    'duration_value': item['duration']['value']
                }
                result['closest_airports'].append(airport_dict)
            except KeyError as e:
                continue

        if request.GET.get('sort_by') != 'quickest':
            result['closest_airports'] = sorted(result['closest_airports'], key=lambda x: x['distance_value'])
        else:
            result['closest_airports'] = sorted(result['closest_airports'], key=lambda x: x['duration_value'])

        radius = int(request.GET.get('radius')) if length_unit == 'kms' else int(radius * self.MILES_DELTA)
        for airport in range(len(result['closest_airports'])):
            if int(''.join(result['closest_airports'][airport]["Airport_distance"].split()[:-1])) > radius:
                result['closest_airports'][airport] = ''

        result['closest_airports'] = [e for e in result['closest_airports'] if e != '']

        for item in result['closest_airports']:
            item.pop('distance_value', None)
            item.pop('duration_value', None)
        return JsonResponse(result)

    @staticmethod
    def _update_airports_details(args):
        request, airport = args
        response = requests.get(
            settings.FLIGHTSTATS_API_SERVICES.format(
                market=request.GET.get('market'),
                currency=request.GET.get('currency_code'),
                locale=request.GET.get('locale'),
                place_id=airport['iata'],
                app_id=settings.FLIGHTSTATS_SERVICES_APP_ID
            )
        )
        response = response.json()

        if "ValidationErrors" not in response:
            airport.update({
                'city_id': response['Places'][0]['CityId'],
                'place_name': response['Places'][0]['PlaceName'],
                'country_id': response['Places'][0]['CountryId'],
                'country_name': response['Places'][0]['CountryName'],
                'PlaceId': response['Places'][0]['PlaceId']
            })
        return airport
