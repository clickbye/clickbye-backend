# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import hashlib
import json
import random
import time

from multiprocessing.pool import ThreadPool

import requests
from django.db import IntegrityError
from django.conf import settings
from django.http import JsonResponse
from rest_framework.views import APIView

from api.models.sky_scanner_cache import SkyScannerFlightsCache
from api.models.sky_scanner_city import CityData
from api.utils import valid_date, is_int, get_unix_timestamp_by_day
from api.utils.image_urls_builder import ImageUrlsBuilder
from api.utils.request_validators import check_param_existing, check_data_by_validator
from api.utils.translated_wishes_getter import TranslatedWishesGetter

list_sort_by = ['cheapest']


class GetFlights(APIView):
    @check_param_existing('skyscanner_city_ids', method='GET')
    @check_param_existing('user_skyscanner_country_id', method='GET')
    @check_param_existing('locale', method='GET')
    @check_data_by_validator('checkin', valid_date, method='GET')
    @check_data_by_validator('checkout', valid_date, method='GET')
    @check_data_by_validator('max_price', is_int, method='GET')
    @check_param_existing('currency_code', method='GET')
    @check_data_by_validator('adults', is_int, method='GET')
    @check_data_by_validator('children', is_int, method='GET')
    @check_data_by_validator('sort_by', lambda x: x in list_sort_by, method='GET')
    @check_param_existing('language', method='GET')
    @check_param_existing('skyscanner_city_id_destination', method='GET')
    def get(self, request):
        list_skyscanner_city_ids = request.GET['skyscanner_city_ids'].split(',')
        list_arguments_thread = [(request, skyscanner_city_id) for skyscanner_city_id in list_skyscanner_city_ids]
        params_or_cache_list = [(int(request.GET['max_price']), self._get_params_and_cache(*e)) for e in
                                list_arguments_thread]

        if len(list_skyscanner_city_ids) > 1:
            pool = ThreadPool(5)
            list_thread_returned = pool.map(self.get_flights, params_or_cache_list)
            pool.close()
            pool.join()

            flights = []
            for row in list_thread_returned:
                if isinstance(row, SkyScannerFlightsCache):
                    row = self.save_cache(row)
                    row = row.flights['flights']
                flights.extend(row)

        else:
            flights = self.get_flights(params_or_cache_list[0])
            if isinstance(flights, SkyScannerFlightsCache):
                print('SHOULD BE SAVED - {}'.format(flights.flights is not None))
                flights = self.save_cache(flights)
                flights = flights.flights['flights']

        skyscanner_city_id_destination = request.GET['skyscanner_city_id_destination']
        skyscanner_obj = CityData.objects.get(skyscanner_id=skyscanner_city_id_destination)
        context = {
            'result': 'success',
            'reason': 'n/a',
            'wishes': self.get_list_wishes(skyscanner_obj, request.GET['language']),
            'flights': flights,
            'temperature': self.get_temperature(flights),
            'url_image': ImageUrlsBuilder().get_destination_url_picture(skyscanner_obj),
            'destination_city': skyscanner_obj.city_name
        }
        return JsonResponse(context)

    def get_flights(self, list_arguments):
        max_price = list_arguments[0]
        params, cache, created = list_arguments[1]
        return self.get_flights_main_query(max_price, params, cache, created)

    def get_flights_main_query(self, max_price, params, cache, created):
        flights_results, was_cache_used = self.get_skyscanner_flights(params, cache, created)
        print("WAS CACHE USED? - {}".format(was_cache_used))
        if not was_cache_used:
            list_itineraries = flights_results.get('Itineraries', [])
            dict_legs = self.convert_list_to_dict_by_id(flights_results.get('Legs', {}))
            dict_carriers = self.convert_list_to_dict_by_id(flights_results.get('Carriers', {}))
            dict_places = self.convert_list_to_dict_by_id_if_not_empty(flights_results.get('Places', {}))
            dict_agents = self.convert_list_to_dict_by_id_if_not_empty(flights_results.get('Agents', {}))

            # Build flights
            list_flights = []
            for elem in list_itineraries:
                price = elem.get('PricingOptions')[0].get('Price')

                if int(float(price)) <= max_price:
                    outbound = dict_legs.get(elem.get('OutboundLegId'))
                    inbound = dict_legs.get(elem.get('InboundLegId'))
                    url_booking = elem.get('PricingOptions')[0].get('DeeplinkUrl')

                    outbound_carrier = dict_carriers.get(outbound.get("Carriers")[0])
                    inbound_carrier = dict_carriers.get(inbound.get("Carriers")[0])

                    agent = dict_agents.get(elem.get('PricingOptions')[0].get('Agents')[0])

                    list_flights.append({
                        'outbound_departure_datetime': outbound.get('Departure'),
                        'outbound_arrival_datetime': outbound.get('Arrival'),
                        'outbound_duration': outbound.get('Duration'),
                        'outbound_departure_location': dict_places.get(outbound.get('OriginStation')),
                        'outbound_arrival_location': dict_places.get(outbound.get('DestinationStation')),

                        'inbound_departure_datetime': inbound.get('Departure'),
                        'inbound_arrival_datetime': inbound.get('Arrival'),
                        'inbound_duration': inbound.get('Duration'),
                        'inbound_departure_location': dict_places.get(inbound.get('OriginStation')),
                        'inbound_arrival_location': dict_places.get(inbound.get('DestinationStation')),

                        'url_booking': url_booking,
                        'flight_price': price,
                        'agent_name': agent.get("Name"),
                        'agent_image_url': agent.get("ImageUrl"),

                        'outbound_carrier_name': outbound_carrier.get('Name'),
                        "outbound_carrier_image_url": outbound_carrier.get('ImageUrl'),
                        'inbound_carrier_name': inbound_carrier.get('Name'),
                        'inbound_carrier_image_url': inbound_carrier.get('ImageUrl'),

                        'outbound_stops': len(outbound.get("Stops")),
                        'inbound_stops': len(inbound.get("Stops")),
                    })

            cache.flights = {'flights': list_flights}
            return cache
        return flights_results

    def get_temperature(self, flights):
        if flights:
            location_name = flights[0].get('outbound_arrival_location').get('Name')
            try:
                temp = None
                coordinates = self.get_coordinates(location_name)
                day = flights[0].get('outbound_arrival_datetime')[:10]
                if coordinates and day:
                    temp = self.get_temperature_by_location(day, coordinates.get('lat'), coordinates.get('lng'))
                return int(float(temp)) if temp else random.randint(19,34)
            except requests.RequestException:
                return random.randint(19, 34)

    def get_list_wishes(self, skyscanner_city_object, language):
        return TranslatedWishesGetter().get_wishes_list(
            skyscanner_city_object.wishes,
            language
        )

    @staticmethod
    def get_coordinates(location_name):
        url = "https://maps.googleapis.com/maps/api/geocode/json?address={}&key={}".format(
            location_name, settings.GOOGLE_MAPS_KEY
        )
        response = requests.get(url, headers={'Accept': "application/json", "Accept-Charset": "utf-8"})
        dict_json = response.json()
        results = dict_json.get('results')
        return results[0].get('geometry').get('location') if results else None

    @staticmethod
    def get_temperature_by_location(day, lat, lng):
        timestamp = get_unix_timestamp_by_day(day)
        url = "https://api.darksky.net/forecast/{}/{},{},{}?units=si".format(
            settings.DARKSKY_LEY, lat, lng, timestamp
        )
        response = requests.get(url, headers={'Accept': "application/json", "Accept-Charset": "utf-8"})
        return response.json().get('currently', {}).get('temperature')


    def convert_list_to_dict_by_id_if_not_empty(self, iterable):
        if iterable:
            return self.convert_list_to_dict_by_id(iterable)
        return dict()

    @staticmethod
    def convert_list_to_dict_by_id(iterable):
        return dict(((e.get('Id'), e) for e in iterable))

    def _build_params_for_skyscanner(self, request, skyscanner_city_id):
        return dict(
            cabinclass='Economy',
            country=request.GET['user_skyscanner_country_id'][:-4],
            currency=request.GET['currency_code'],
            locale=request.GET['locale'],
            language=request.GET['language'],
            originplace=skyscanner_city_id,
            destinationplace=request.GET['skyscanner_city_id_destination'],
            outbounddate=request.GET['checkin'],
            inbounddate=request.GET['checkout'],
            adults=request.GET['adults'],
            children=request.GET['children'],
            infants='0',
            apiKey=settings.SKYSCANNER_API_KEY
        )

    def _get_params_and_cache(self, request, skyscanner_city_id):
        params = self._build_params_for_skyscanner(request, skyscanner_city_id)
        params_hash = self._get_hash(params)
        cache = SkyScannerFlightsCache.objects.filter(query=params_hash)
        if cache:
            cache = cache[0]
            created = False
        else:
            cache = SkyScannerFlightsCache(query=params_hash)
            created = True

        return params, cache, created

    def _get_hash(self, data):
        return hashlib.md5(json.dumps(data, sort_keys=True).encode('utf-8')).hexdigest()

    def get_skyscanner_flights(self, params, cache, created):
        session_key = self.get_skyscanner_session_key(params)
        return self._get_sesion_flights(session_key, cache, created)

    def _get_sesion_flights(self, session_key, cache, created):
        print("CREATED - {}".format(created))
        for i in range(20):
            response = requests.get(session_key, headers={
                'Accept': "application/json",
                "Accept-Charset": "utf-8",
                'Cache-Control': "no-cache"
            })
            if response.ok:
                if response.status_code == 304 and cache.flights.get('flights'):
                    return cache.flights.get('flights'), True

                elif response.status_code != 304:
                    return response.json(), False

                elif i == 20:
                    raise Exception('Skyscanner forces to use cache but the cache is empty')
            time.sleep(3)
        raise Exception('Getting Skyscanner session url failed after 20 tries')

    def get_skyscanner_session_key(self, params):
        url = "http://partners.api.skyscanner.net/apiservices/pricing/v1.0"
        payload = '&'.join(["{}={}".format(k, v) for k, v in params.items()])
        headers = {
            'Content-Type': "application/x-www-form-urlencoded",
            'Accept': 'application/json',
            "Accept-Charset": "utf-8",
            'Cache-Control': "no-cache",
        }

        for i in range(4):
            response = requests.request("POST", url, data=payload, headers=headers)
            location = response.headers.get('location')
            if location is not None:
                return location + "?apiKey=" + settings.SKYSCANNER_API_KEY + "&pageIndex=0&pageSize=10"
            time.sleep(.5)
        raise Exception('Getting Skyscanner session url failed after 3 tries')

    def save_cache(self, cache):
        try:
            cache.save()
        except IntegrityError as e:
            pass
        return cache
