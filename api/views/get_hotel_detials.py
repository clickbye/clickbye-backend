# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import random
import string
from datetime import datetime

from django.http import JsonResponse
from rest_framework.views import APIView

from api.models.sky_scanner_city import CityData
from api.utils import valid_date, is_int
from api.utils.booking_com_affiliate import BookingcomAffiliate
from api.utils.hotels_utils import generate_room_symbol
from api.utils.image_urls_builder import ImageUrlsBuilder
from api.utils.request_validators import check_param_existing, check_data_by_validator


class GetHotelsDetails(APIView):
    @check_param_existing('skyscanner_city_id_destination', method='GET')
    @check_data_by_validator('checkin', valid_date, method='GET')
    @check_data_by_validator('checkout', valid_date, method='GET')
    @check_data_by_validator('max_price', is_int, method='GET')
    @check_data_by_validator('stars', is_int, method='GET')
    @check_data_by_validator('page', is_int, method='GET')
    @check_data_by_validator('rows', is_int, method='GET')
    @check_param_existing('currency_code', method='GET')
    @check_data_by_validator('adults', is_int, method='GET')
    @check_data_by_validator('children', is_int, method='GET')
    @check_param_existing('language', method='GET')
    def get(self, request):
        stars = int(request.GET['stars'])
        stars_to_search = -1 if stars != 5 else 5

        page = int(request.GET['page'])
        page = 1 if page < 1 else page

        checkin = request.GET['checkin']
        checkout = request.GET['checkout']

        bcom = BookingcomAffiliate()
        skyscanner_data = CityData.objects.get(skyscanner_id=request.GET['skyscanner_city_id_destination'])
        destination_city_image = ImageUrlsBuilder().get_destination_url_picture(skyscanner_data)

        rows = int(request.GET['rows'])
        offset = (page - 1) * rows

        room_symbol = generate_room_symbol(request.GET['adults'], request.GET['children'])

        max_price = int(request.GET['max_price'])
        days_between_dates = self._calculate_days_between_date(checkin, checkout)
        max_price_per_night = int(max_price / days_between_dates)

        list_hotels = bcom.get_hotel_availability_v2(
            offset,
            rows,
            skyscanner_data.bcom_id,
            checkin,
            checkout,
            max_price_per_night,
            request.GET['currency_code'],
            room_symbol,
            stars_to_search
        ).get('hotels')

        booking_com_url_params = self.create_url_for_hotel(
            checkin,
            checkout,
            request.GET['adults'],
            request.GET['children'],
            room_symbol
        )

        booking_com_all_hotels_params = "{}&city={}".format(booking_com_url_params, skyscanner_data.bcom_id)

        if list_hotels:
            string_list_hotels = self._get_hotels_ids_as_string(list_hotels)
            data_hotels = bcom.get_hostels_data(string_list_hotels, request.GET['language'])

            data_pictures_hotels = bcom.get_hotel_description_photos(string_list_hotels)
            dict_list_hotels = self._get_hotels_dict(list_hotels)

            hotels_to_return = []
            for elem in data_hotels:
                elem['url'] = "{}?{}".format(elem.get('url'), booking_com_url_params)

                if elem['class'] < stars:
                    continue

                if elem.get('hotel_id') in dict_list_hotels:
                    if elem.get('hotel_id') in data_pictures_hotels:
                        elem['pictures'] = data_pictures_hotels.get(elem.get('hotel_id'))

                    elem['price'] = dict_list_hotels[elem['hotel_id']]
                    elem['price']['number_of_nights'] = days_between_dates
                    hotels_to_return.append(elem)

            context = {
                'result': 'success',
                'reason': 'n/a',
                'data_hotels': sorted(hotels_to_return, key=lambda k: k['price']['price'], reverse=True),
                'image_url': destination_city_image,
                'booking_id': skyscanner_data.bcom_id,
                'destination_city_name': skyscanner_data.city_name,
                'all_hotels_url': "http://www.booking.com/searchresults.html?{}".format(booking_com_all_hotels_params)
            }

        else:
            context = {
                'result': 'success',
                'reason': 'zero_hotels',
                'data_hotels': [],
                'image_url': destination_city_image,
                'booking_id': skyscanner_data.bcom_id,
                'destination_city_name': skyscanner_data.city_name,
                'all_hotels_url': "http://www.booking.com/searchresults.html?{}".format(booking_com_all_hotels_params)
            }

        return JsonResponse(context)

    def create_url_for_hotel(self, checkin, checkout, adults, children, room_symbol):
        sid = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(32))
        checkin = self._create_check_date(checkin, date_type='checkin')
        checkout = self._create_check_date(checkout, date_type='checkout')

        return "aid=1218858{}{}&no_rooms=1&group_adults={}&group_children={}&room1={}&sid={}".format(
            checkin,
            checkout,
            adults,
            children,
            self.encoded_room_symbol_for_url(room_symbol),
            sid
        )

    @staticmethod
    def _calculate_days_between_date(checkin, checkout):
        date_format = "%Y-%m-%d"
        a = datetime.strptime(checkin, date_format)
        b = datetime.strptime(checkout, date_format)
        return (b - a).days or 1

    @staticmethod
    def _create_check_date(date, date_type):
        return "&{date_type}_monthday={day}&{date_type}_month={month}&{date_type}_year={year}".format(
            date_type=date_type,
            day=date[8:],
            month=date[5:7],
            year=date[0:4]
        )

    @staticmethod
    def encoded_room_symbol_for_url(room_symbol):
        string_room = ''
        for elem in room_symbol.split(','):
            string_room += ",12" if is_int(elem) else ",A"

        return string_room[1:].replace(',', '%2C')

    @staticmethod
    def _get_hotels_ids_as_string(list_hotels):
        return ','.join([row.get('hotel_id') for row in list_hotels])

    @staticmethod
    def _get_hotels_dict(list_hotels):
        return dict((row.get('hotel_id'), row) for row in list_hotels)
