# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse
from rest_framework.views import APIView

from api.models.sky_scanner_city import CityData
from api.utils import valid_date, is_int
from api.utils.hotel_price_getter import HotelPriceGetter
from api.utils.request_validators import check_param_existing, check_data_by_validator


class GetMostExpensiveHotels(APIView):
    @check_param_existing('skyscanner_city_ids', method='GET')
    @check_data_by_validator('checkin', valid_date, method='GET')
    @check_data_by_validator('checkout', valid_date, method='GET')
    @check_param_existing('max_prices', method='GET')
    @check_data_by_validator('stars', is_int, method='GET')
    @check_param_existing('currency_code', method='GET')
    @check_data_by_validator('adults', is_int, method='GET')
    @check_data_by_validator('children', is_int, method='GET')
    def get(self, request):
        list_max_prices = request.GET['max_prices'].split(',')
        list_skyscanner_city_ids = request.GET['skyscanner_city_ids'].split(',')
        prices_are_correct = all(is_int(p) for p in list_max_prices)

        if len(list_max_prices) == len(list_skyscanner_city_ids) and prices_are_correct:

            stars = int(request.GET['stars'])
            if stars < 1 or stars > 5:
                stars = -1

            dict_results = {}

            for index, skyscanner_city_id in enumerate(list_skyscanner_city_ids):
                # TODO - make one DB connection here
                skyscanner_data_obj = CityData.objects.get(skyscanner_id=skyscanner_city_id)
                max_price = int(list_max_prices[index])
                dict_results[skyscanner_city_id] = HotelPriceGetter().get_hotel_price(
                    'most_expensive',
                    skyscanner_data_obj,
                    max_price,
                    request.GET['adults'],
                    request.GET['children'],
                    request.GET['checkin'],
                    request.GET['checkout'],
                    request.GET['currency_code'],
                    stars
                )

            return JsonResponse({
                'result': 'success',
                'reason': 'n/a',
                'prices': dict_results
            })

        return JsonResponse({
                'result': 'error',
                'reason': 'parameters_invalid_format'
            }, status=400)
