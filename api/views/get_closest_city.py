# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import collections
import json

import requests
from django.conf import settings
from django.http import JsonResponse
from rest_framework.views import APIView

from api.utils.request_validators import check_param_existing


class GetClosestCity(APIView):
    @check_param_existing('ip_address', method='GET')
    @check_param_existing('market', method='GET')
    @check_param_existing('locale', method='GET')
    @check_param_existing('locale', method='GET')
    def get(self, request):
        result = collections.OrderedDict()

        response = requests.get(
            settings.FLIGHTSTATS_API_SERVICES.format(
                market=request.GET.get('market'),
                currency=request.GET.get('currency_code'),
                locale=request.GET.get('locale'),
                place_id='{0}-ip'.format(request.GET.get('ip_address')),
                app_id=settings.FLIGHTSTATS_SERVICES_APP_ID
            )
        )
        flg_response = json.loads(response.content)

        if u"ValidationErrors" in flg_response.keys():
            return JsonResponse(flg_response)

        google_resp = requests.get(
            settings.GOOGLE_MAPS_GEOCODE_API.format(
                locale=request.GET.get('locale'),
                address=flg_response['Places'][0]['PlaceName'],
                key=settings.GOOGLE_MAPS_KEY
            )
        )
        google_resp = json.loads(google_resp.content)

        if google_resp['status'] != 'OK':
            return JsonResponse(google_resp)

        result['closest_city'] = []
        result['result'] = 'success'
        result['reason'] = 'n/a'

        city_dict = collections.OrderedDict()
        city_dict['city_name'] = flg_response['Places'][0]['PlaceName']
        city_dict['skyscanner_place_id'] = flg_response['Places'][0]['PlaceId']
        city_dict['country_id'] = flg_response['Places'][0]['CountryId']
        city_dict['country_name'] = flg_response['Places'][0]['CountryName']
        city_dict['skyscanner_city_id'] = flg_response['Places'][0]['CityId']
        city_dict['latitude'] = google_resp['results'][0]['geometry']['location']['lat']
        city_dict['longitude'] = google_resp['results'][0]['geometry']['location']['lng']

        result['closest_city'].append(city_dict)
        return JsonResponse(result)
