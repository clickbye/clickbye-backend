# -*- coding: utf-8 -*- 
from __future__ import unicode_literals

import requests


class BookingcomAffiliate(object):
    FR = ['Excellent', 'Très Bien', 'Bien', 'Agréable']
    EN = ['Great', 'Very Good', 'Good', 'Pleasant']
    IT = ['Eccelente', 'Ottimo', 'Buono', 'Carino']
    PT = ['Soberbo', 'Muito bom', 'Bom', 'Agradável']
    ES = ['Fantástico', 'Muy bien', 'Bien', 'Agradable']
    DE = ['Hervorragend', 'Sehr gut', 'Gut', 'Ansprechend']
    RU = ['Превосходно', 'Очень хорошо', 'Хорошо', 'Достаточно хорошо']

    TRANSLATION_MAPPER = {
        'fr': FR,
        'en': EN,
        'it': IT,
        'pt': PT,
        'es': ES,
        'de': DE,
        'ru': RU
    }

    def __init__(self):
        self.username = "clickbye"
        self.password = "clickbyeapi"

    def make_query(self, api, params):
        result = requests.post(
            "https://distribution-xml.booking.com/json/{}".format(api),
            auth=(self.username, self.password),
            data=params
        )
        return result.json()

    def get_hotel_availability_v2(self, offset, rows, city_ids, checkin, checkout, max_price, currency_code, room1,
                                  stars=-1):
        params = {
            "rows": rows,
            "offset": offset,
            "city_ids": city_ids,
            "checkin": checkin,
            "checkout": checkout,
            "max_price": max_price,
            "affiliate_id": "1218858",
            "order_by": "ranking",
            "currency_code": currency_code,
            "room1": room1,
        }

        if 1 <= stars <= 5:
            params["stars"] = stars

        return self.make_query("getHotelAvailabilityV2", params)

    def get_hostels_data(self, hotel_ids, languagecode):
        params = {
            "hotel_ids": hotel_ids,
            "languagecode": languagecode,
        }
        content = self.make_query("bookings.getHotels", params)

        for row in content:
            row['hotel_id'] = str(row['hotel_id'])
            if 'review_score' in row:
                row['review_word'] = self.get_review_word(languagecode, row['review_score'])

        return content

    # Get the photos for the hotels
    def get_hotel_description_photos(self, hotel_ids):
        params = {
            "hotel_ids": hotel_ids,
        }
        content = self.make_query("bookings.getHotelDescriptionPhotos", params)

        dict_pictures = {}
        for elem in content:
            dict_pictures[elem.get('hotel_id')] = elem

        return dict_pictures

    @staticmethod
    def get_average_hotel_price_from_list(list_hotels):
        total = 0
        len_list_hotels = len(list_hotels)

        if len_list_hotels >= 1:

            for elem in list_hotels:
                total = total + float(elem.get('price'))

        return int(total / len_list_hotels)

    @staticmethod
    def get_cheapest_hotel_price_from_list(list_hotels):
        hotel_chosen = None

        if len(list_hotels) >= 1:
            hotel_chosen = list_hotels[0]

            for elem in list_hotels[1:]:
                if float(elem.get('price')) < float(hotel_chosen.get('price')):
                    hotel_chosen = elem

        return hotel_chosen.get('price')

    @staticmethod
    def get_most_expensive_hotel_price_from_list(list_hotels):
        hotel_chosen = None

        if len(list_hotels) >= 1:
            hotel_chosen = list_hotels[0]

            for elem in list_hotels[1:]:
                if float(elem.get('price')) > float(hotel_chosen.get('price')):
                    hotel_chosen = elem

        return hotel_chosen.get('price')

    @staticmethod
    def get_review_word(language, score):

        score = float(score)
        language = language.lower()

        default = BookingcomAffiliate.TRANSLATION_MAPPER['en']
        list_to_use = BookingcomAffiliate.TRANSLATION_MAPPER.get(language, default)

        if score >= 9:
            return list_to_use[0]

        elif score >= 8:
            return list_to_use[1]

        elif score >= 7:
            return list_to_use[2]

        elif score >= 6:
            return list_to_use[3]

        return ''
