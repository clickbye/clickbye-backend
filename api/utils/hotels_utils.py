def generate_room_symbol(adults, children):
    return 'A,' * int(adults) + '12,' * int(children)
