import time

from api.utils.booking_com_affiliate import BookingcomAffiliate
from api.utils.hotels_utils import generate_room_symbol


class HotelPriceGetter(object):
    def get_hotel_price(
            self,
            hotels_selection,
            skyscanner_data_obj,
            max_price,
            adults,
            children,
            checkin,
            checkout,
            currency_code,
            stars
    ):
        price = None
        bcom = BookingcomAffiliate()
        city_id = skyscanner_data_obj.bcom_id

        if city_id is not None:
            max_price = int(max_price)
            room1 = generate_room_symbol(adults, children)

            try:
                list_hotels = bcom.get_hotel_availability_v2(0, 5, city_id, checkin, checkout, max_price, currency_code,
                                                             room1,
                                                             stars).get('hotels')
            except ValueError:
                time.sleep(3)
                list_hotels = bcom.get_hotel_availability_v2(0, 5, city_id, checkin, checkout, max_price, currency_code,
                                                             room1,
                                                             stars).get('hotels')

            if list_hotels:
                if hotels_selection == 'cheapest':
                    price_found = bcom.get_cheapest_hotel_price_from_list(list_hotels)

                elif hotels_selection == 'average':
                    price_found = bcom.get_average_hotel_price_from_list(list_hotels)

                else:
                    price_found = bcom.get_most_expensive_hotel_price_from_list(list_hotels)

                price_found = int(float(price_found))
                if price_found <= max_price:
                    price = price_found

        return price
