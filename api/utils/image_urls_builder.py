import random
from django.conf import settings


class ImageUrlsBuilder:
    BASE_URL = settings.BUCKETEER_URL
    STATIC_WISHES = "static_wishes/"
    STATIC_DESTINATIONS = "url_z/"

    WISHES_MAPPER = {
        0: "Art-%2526-Culture.png",
        1: "Beach.png",
        2: "Budget-Friendly.png",
        3: "Family.png",
        4: "Nature.png",
        5: "Party.png",
        6: "Romance.png",
        7: "City-Break.png"
    }

    def get_wisht_url_picture(self, pos):
        if pos in self.WISHES_MAPPER:
            return "{}{}{}".format(self.BASE_URL, self.STATIC_WISHES, self.WISHES_MAPPER[pos])
        return ""

    def get_destination_url_picture(self, skyscanner_obj):
        if skyscanner_obj.max_pics > 0:
            rand_number = random.randint(1, skyscanner_obj.max_pics)
            return "{}{}{}/{}/{}.jpg".format(
                self.BASE_URL,
                self.STATIC_DESTINATIONS,
                skyscanner_obj.country_id,
                skyscanner_obj.skyscanner_id,
                rand_number
            )
