# -*- coding: utf-8 -*-
from api.utils.image_urls_builder import ImageUrlsBuilder


list_EN_lowercase = ['arts & culture', 'beach', 'budget-friendly', 'family', 'nature', 'party', 'romance',
                     'city break']

EN = ['Arts & Culture', 'Beach', 'Budget-friendly', 'Family', 'Nature', 'Party', 'Romance', 'City break']
FR = ['Arts & culture', 'Plage', 'Petit Budget', 'En famille', 'Nature', 'Entre potes', 'En couple',
      'City break']
IT = ['Arte e cultura', 'Spiaggia', 'Economico', 'Famiglia', 'Natura', 'Partito', 'Romantico', 'City Break']
ES = ['Arte y Cultura', 'playa', 'Económico', 'Familia', 'Naturaleza', 'Fiesta', 'Romántico', 'City break']
PT = ['Artes e cultura', 'Praia', 'Econômico', 'Família', 'Natureza', 'Festa', 'Romance', 'City break']
DE = ['Kunst und Kultur', 'Strand', 'Budget-freundlich', 'Familie', 'Natur', 'Party', 'Romantik',
      'Städtereise']
RU = ['Искусство и культура', 'Пляж', 'Бюджет-дружественный', 'Семья', 'Природа', 'Партия', 'Романтика',
      'Городской перерыв']

translations = {
    'fr': FR,
    'it': IT,
    'es': ES,
    'pt': PT,
    'de': DE,
    'ru': RU,
    'en': EN,
}


class TranslatedWishesGetter(object):
    def get_wishes_list(self, skyscanner_obj_wishes, language):
        list_wishes = []
        for wish_en in skyscanner_obj_wishes:
            wish_translation, wish_pos = self._get_wishes_translation(wish_en, language)

            if wish_translation:
                list_wishes.append({
                    'name': wish_translation,
                    'image_url': ImageUrlsBuilder().get_wisht_url_picture(wish_pos),
                    'pos': wish_pos
                })
        return sorted(list_wishes, key=lambda x: x['name'])

    def _get_wishes_translation(sefl, wish_en, language):
        try:
            pos = list_EN_lowercase.index(wish_en.lower())
        except ValueError:
            pos = -1

        if pos != -1 and translations.get(language):
            return translations[language][pos], pos
        return None, -1
