from rest_framework.response import Response


def prepare_error_response(message, status_code, data=None):
    data = data if data else {}
    data.update({
        'message': message,
    })
    return _response(status_code, data)


def prepare_success_response(data=None, status_code=200):
    return _response(status_code, data)


def _response(status_code, data):
    response = {
        'status': 'success' if 200 <= status_code < 400 else 'error',
        'statusCode': status_code,
        'data': data,
    }
    return Response(response, status=status_code)
