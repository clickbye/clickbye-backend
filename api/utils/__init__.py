import datetime
import os
import random
import string
import time

import openpyxl
from django.conf import settings
from django.db import IntegrityError


def generate_code(length=25):
    """
    Generate random code with upper, lower case chars and digits
    :param length: length of code
    :return: new generated code
    """
    code = ''.join([random.choice(string.ascii_uppercase
                                  + string.ascii_lowercase
                                  + string.digits) for _ in range(length)])
    return code


def get_unix_timestamp_by_day(day):
    dt = datetime.datetime.strptime(day, '%Y-%m-%d')
    return int(time.mktime(dt.timetuple()))


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def valid_date(datestring):
    try:
        datetime.datetime.strptime(datestring, '%Y-%m-%d')
        return True
    except ValueError:
        return False
