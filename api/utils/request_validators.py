from functools import wraps

from rest_framework.request import Request

from .response_format import prepare_error_response


def check_param_existing(param_name,
                         error_msg='Missing parameter {}',
                         status_code=400,
                         method='POST'):
    """
    Checks if param in request exists
    :param param_name: Parameter's name which should exist in request
    :param error_msg: Error message which will be sent in Response
    :param status_code: Error status code which will be sent in Response
    :param method: Request method name

    Intended to be used with API View methods.
    Expects the method to be passed request as a second argument (implying that 'self' is always the first)
    """
    def _my_decorator(api_view_method):

        @wraps(api_view_method)
        def _decorator(*args, **kwargs):
            request = args[1]
            if not isinstance(request, Request):
                raise TypeError('Expected rest_framework.request.Request as a second argument')

            param_dicts = {
                'GET': request.query_params,
                'POST': request.data,
                'POST_MULTIPART': request.FILES,
            }
            if not param_dicts[method].get(param_name):
                response = prepare_error_response(error_msg.format(param_name), status_code)
            else:
                response = api_view_method(*args, **kwargs)
            return response

        return _decorator
    return _my_decorator


def check_data_by_validator(
        param_name,
        validator,
        method='POST',
        error_msg='Check your data for {}',
        status_code=400,
        data=None

):
    """
    Checks if request data is valid by passed form
    :param param_name: Parameter's name which should exist in request
    :param validator: Validator function
    :param error_msg: Error message which will be sent in Response
    :param status_code: Error status code which will be sent in Response
    :param data: Error data which will be sent in Response, by default validation error is sent

    Intended to be used with API View methods.
    Expects the method to be passed request as a second argument (implying that 'self' is always the first)
    """
    def _my_decorator(api_view_method):

        @wraps(api_view_method)
        def _decorator(*args, **kwargs):
            request = args[1]
            if not isinstance(request, Request):
                raise TypeError('Expected rest_framework.request.Request as a second argument')

            param_dicts = {
                'GET': request.query_params,
                'POST': request.data,
                'POST_MULTIPART': request.FILES,
            }
            if not param_dicts[method].get(param_name):
                return prepare_error_response(error_msg.format(param_name), status_code)

            is_valid = validator(param_dicts[method].get(param_name))
            if not is_valid:
                response_data = data if data else {'validation': "{} has wrong format".format(param_name)}
                return prepare_error_response(error_msg.format(param_name), status_code, response_data)
            else:
                response = api_view_method(*args, **kwargs)
            return response

        return _decorator
    return _my_decorator
