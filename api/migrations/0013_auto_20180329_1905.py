# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-03-29 19:05
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0012_auto_20180315_1920'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='SkyScannerData',
            new_name='CityData',
        ),
    ]
