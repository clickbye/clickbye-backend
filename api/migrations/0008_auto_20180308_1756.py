# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-03-08 17:56
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_auto_20180308_1514'),
    ]

    operations = [
        migrations.CreateModel(
            name='SkyScannerFlightsCache',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('query', models.CharField(db_index=True, max_length=200, unique=True)),
                ('flights', django.contrib.postgres.fields.jsonb.JSONField()),
                ('modified', models.DateTimeField(auto_now=True, db_index=True, verbose_name='date_created')),
            ],
        ),
        migrations.DeleteModel(
            name='SkyScannerCache',
        ),
    ]
