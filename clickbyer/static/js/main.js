
(function ($) {
    "use strict";

    /*==================================================================
    [ Validate ]*/
    var new_pwd = $('#new_pwd');
    var confirm_pwd = $('#confirm_pwd');
    var error_box = $('#error-box');
    var empty_password_error_box = $('#empty-password-error-box');
    var short_password_error_box = $('#short-password-error-box');
    var not_the_same_passwords_box = $('#not-the-same-passwords-error-box');
    var wrong_reset_code_error_box = $('#wrong-reset-code-error-box');
    var success_box = $('#success-box');

    $('#pwd_button').click(function() {
        error_box.hide();
        empty_password_error_box.hide();
        short_password_error_box.hide();
        not_the_same_passwords_box.hide();
        wrong_reset_code_error_box.hide();
        success_box.hide();

        var new_pwd_val = new_pwd.val();
        var confirm_pwd_val = confirm_pwd.val();

        function success() {
            success_box.show();
        }

        function error(a) {
            error_box.html("<strong>Error!</strong> " + a.responseText);
            error_box.show();
        }

       if(new_pwd_val.length === 0) {
           empty_password_error_box.show();

       } else if(new_pwd_val.length < 8){
           short_password_error_box.show();

       } else if(new_pwd_val !== confirm_pwd_val) {
           not_the_same_passwords_box.show()

       } else {
           var code = $.cookie("reset_code");
           if(code === undefined) {
               wrong_reset_code_error_box.show();
               return
           }
           var url = "https://www.clickbye.fun/api/user/password/reset/" + code + "/";
           $.ajax({
              type: "POST",
              url: url,
              data: {
                  new_password: new_pwd_val,
              },
              success: success,
              error: error
            });
       }
    });

    

})(jQuery);