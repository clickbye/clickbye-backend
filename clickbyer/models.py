from __future__ import unicode_literals

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


class FlightUser(AbstractUser):
    GENDER_CHOICES = (
        ('female', 'female'),
        ('male', 'male')
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'username']

    email = models.EmailField(
        'Email',
        max_length=255,
        unique=True,
        null=False,
        blank=False
    )

    gender = models.CharField(max_length=6, choices=GENDER_CHOICES)
    birth_year = models.IntegerField(null=True, blank=True)

    country = models.CharField(max_length=50, null=True, blank=True)
    region = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=255, null=True, blank=True)
    home_city = models.CharField(max_length=255, null=True, blank=True)

    locale = models.CharField(max_length=10, null=True, blank=True)
    reg_type = models.CharField(max_length=255, null=True, blank=True)

    timezone = models.CharField(max_length=255, null=True, blank=True)
    signed_up = models.DateTimeField(auto_now_add=True)
    last_seen = models.DateTimeField(auto_now_add=True)

    anonymous_id = models.CharField(max_length=255, null=True, blank=True)

    device_type = models.CharField(max_length=255, null=True, blank=True)
    device_manufacturer = models.CharField(max_length=255, null=True, blank=True)
    device_model = models.CharField(max_length=255, null=True, blank=True)

    os_name = models.CharField(max_length=255, null=True, blank=True)
    os_version = models.CharField(max_length=255, null=True, blank=True)

    app_name = models.CharField(max_length=255, blank=True, null=True)
    app_version = models.CharField(max_length=255, blank=True, null=True)

    social_network = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        verbose_name = 'clickbyer'
        verbose_name_plural = 'clickbyers'
        db_table = 'flightuser'

    def __str__(self):
        return self.first_name + " " + self.last_name

    @property
    def name(self):
        return "{0} {1}".format(self.first_name, self.last_name)

    @property
    def connection_type(self):
        return self.social_network.capitalize() if self.social_network else 'Mail'

    @property
    def is_eng(self):
        return self.locale != 'fr_FR'


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.get_or_create(user=instance)


class OldAccountEmailList(models.Model):
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=100)
    first_email_sent_date = models.DateField(null=True, blank=True)
    language = models.CharField(max_length=2, null=True, blank=True)
    gender = models.CharField(max_length=20, null=True, blank=True)
    birth_year = models.PositiveIntegerField(null=True, blank=True)


    def __str__(self):
        return "{} - {}".format(self.email, self.first_email_sent_date)

    @property
    def is_eng(self):
        return self.language != 'fr'

    @property
    def was_email_sent(self):
        return bool(self.first_email_sent_date)
