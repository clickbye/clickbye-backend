from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.validators import validate_email


def check_email(email):
    if get_user_model().objects.filter(email=email).exists():
        return False, 'Email already registered'
    try:
        validate_email(email)
    except ValidationError:
        return False, "Wrong email's format"
    return True, 'OK'


def check_username(username):
    if get_user_model().objects.filter(username=username).exists():
        return False, "Username already registered"
    return True, 'OK'


def check_password(password):
    return len(password) >= 8
