from django.contrib import admin

from .models import FlightUser


@admin.register(FlightUser)
class FlightUserAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'email',
        'username',
        'signed_up',
        'last_seen',
        'name',
        'gender',
        'birth_year',
        'home_city',
        'connection_type',
        'region',
        'city',
        'timezone',
        'locale',
        'device_type',
        'device_manufacturer',
        'device_model',
        'os_name',
        'app_name'
    ]
    readonly_fields = ['password']
