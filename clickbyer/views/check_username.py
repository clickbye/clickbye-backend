# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse
from rest_framework.views import APIView

from api.utils.request_validators import check_param_existing
from clickbyer.utils import check_username


class CheckUsername(APIView):
    @check_param_existing('username', method='GET')
    def get(self, request):
        is_valid_username, error = check_username(request.GET.get('username'))
        if not is_valid_username:
            return JsonResponse(data={
                "result": "error",
                "reason": error
            }, status=400)

        return JsonResponse(data={
            'Check username': {
                'reason': u'n/a',
                'result': u'success'
            }
        })
