# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def password_reset_template(request, code):
    response = render(request, "reset_password.html")
    response.set_cookie(key='reset_code', value=code)
    return response
