# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.timezone import now
from django.contrib.auth import authenticate
from django.http import JsonResponse
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView

from api.utils.request_validators import check_param_existing
from clickbyer.models import FlightUser
from clickbyer.serializers import FlightUserSerializer
from clickbyer.utils import check_email


class TokenLogin(APIView):
    @check_param_existing('email')
    @check_param_existing('password')
    def post(self, request):
        email = request.POST.get('email')
        password = request.POST.get('password')

        email_check, email_error = check_email(request.POST.get('email'))
        if not email_check and email_error != 'Email already registered':
            return JsonResponse(data={
                "result": "error",
                "reason": email_error
            }, status=400)

        elif email_check:
            return JsonResponse(data={
                "result": "error",
                "reason": "Email does not exist in the database"
            }, status=400)

        email = self._get_correct_email(email)

        user = authenticate(email=email, password=password)
        if not user:
            return JsonResponse(data={
                'result': 'error',
                'reason': 'Wrong combination Email/Password'
            }, status=400)

        user.last_seen = now()
        user.save()

        token, created = Token.objects.get_or_create(user=user)
        user = FlightUserSerializer(instance=user)
        data = {
            'login_user': {
                'reason': u'n/a',
                'result': u'success'
            },
            'token': token.key
        }
        data.update(user.data)
        return JsonResponse(data=data, safe=False)

    def _get_correct_email(self, email):
        user = FlightUser.objects.filter(email__iexact=email)
        return user[0].email if user else email
