# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import authenticate
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated


from api.utils.request_validators import check_param_existing
from clickbyer.utils import check_password


class UpdateUserPassword(APIView):
    permission_classes = (IsAuthenticated,)

    @check_param_existing('old_password')
    @check_param_existing('new_password', check_password)
    def post(self, request):
        user = authenticate(
            email=request.user.email,
            password=request.POST.get('old_password')
        )

        if not user:
            return JsonResponse(data={
                'result': 'error',
                'reason': 'account does not exist'
            }, status=400)

        user.set_password(request.POST.get('new_password'))
        user.save()

        return JsonResponse(data={
            'Update_user_password': {
                'reason': u'n/a',
                'result': u'success'
            }
        })
