# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from uuid import uuid4
from datetime import datetime

import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from django.http import JsonResponse
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from clickbyer.views_mixin.welcome_campaign_mixin import WelcomeCampaignMixin

from api.utils.request_validators import check_param_existing, check_data_by_validator


class ConnectUserSocialNetwork(APIView, WelcomeCampaignMixin):
    @check_param_existing('token')
    @check_data_by_validator('social_network', lambda x: x in ('google', 'facebook'))
    def post(self, request):
        first_time = True
        flight_user_cls = get_user_model()
        currency = ""

        if request.POST.get('social_network') == 'facebook':
            response = requests.get(
                url=settings.FACEBOOK_USER_URL,
                params={
                    "fields": 'email, first_name, last_name, location, gender, birthday, currency',
                    "access_token": request.POST.get('token')
                }
            )
            response_content = response.json()

            if not response.ok or response_content.get('email') is None:
                return JsonResponse(response_content, status=400)

            currency = response_content.get('currency', {}).get('user_currency', '')

            if flight_user_cls.objects.filter(email=response_content.get('email')).exists():
                user = flight_user_cls.objects.get(email=response_content.get('email'))
                first_time = False
            else:
                user = flight_user_cls.objects.create(
                    first_name=response_content.get('first_name', ""),
                    last_name=response_content.get('last_name', ""),
                    gender=response_content.get('gender', ""),
                    email=response_content.get('email', ""),
                    birth_year=self._extract_birth_year_for_facebook(response_content.get('birthday')),
                    home_city=response_content['location']['name'] if response_content.get('location', '') else "",
                    username=response_content['id'],
                    social_network='facebook'
                )
                password = flight_user_cls.objects.make_random_password()
                user.set_password(password)
                user.save()

        else:
            response = requests.get(
                url=settings.GOOGLE_ID_TOKEN_URL,
                params={
                    "id_token": request.POST.get('token')
                }
            )

            response_content = response.json()
            if not response.ok or response_content.get('email') is None:
                return JsonResponse({
                    'result': 'error',
                    'reason': 'Token is invalid or there is no email'
                }, status=400)

            if flight_user_cls.objects.filter(email=response_content.get('email')).exists():
                user = flight_user_cls.objects.get(email=response_content.get('email'))
                first_time = False
            else:
                user = flight_user_cls.objects.create(
                    first_name=response_content.get('given_name', ""),
                    last_name=response_content.get('family_name', ""),
                    gender=response_content.get('gender', ""),
                    email=response_content.get('email', ""),
                    username=str(uuid4()),
                    social_network='google'
                )
                password = flight_user_cls.objects.make_random_password()
                user.set_password(password)
                user.save()

        if first_time:
            self.setup_welcome_campaign(user.pk)

        token, created = Token.objects.get_or_create(user=user)

        return JsonResponse(data={
            'connect_user_social': {
                'first_name': user.first_name,
                'last_name': user.last_name,
                'gender': user.gender,
                'birth_year': user.birth_year,
                'home_city': user.home_city,
                'username': user.username,
                'email': user.email,
                'currency_code': currency,
                'id': user.pk,
                'first_time': "Yes" if first_time else "No",
                'token': token.key
            },
            'reason': u'n/a',
            'result': u'success'
        })

    def _extract_birth_year_for_facebook(self, birthdate):
        formats = ["%m/%d/%Y", "%d/%m/%Y"]
        if birthdate:
            for f in formats:
                try:
                    return datetime.strptime(birthdate, f).year
                except ValueError:
                    pass
