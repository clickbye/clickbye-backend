# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.contrib.auth import get_user_model
from django.http import JsonResponse
from django.template.loader import render_to_string
from rest_framework.views import APIView

from api.models.reset_code import ResetCode
from clickbyer.tasks.send_reset_password_email import send_reset_password_email
from clickbyer.utils import check_email


class PasswordReset(APIView):
    def post(self, request, code=False):
        if not code:
            is_email_valid, email_error = check_email(request.POST.get('email'))
            if not is_email_valid and email_error != 'Email already registered':
                return JsonResponse(data={
                    "result": "error",
                    "reason": email_error
                }, status=400)

            elif is_email_valid:
                return JsonResponse(data={
                    "result": "error",
                    "reason": "Email does not exist in the database"
                }, status=400)

            user = get_user_model().objects.get(email__iexact=request.POST.get('email'))

            code, created = ResetCode.objects.get_or_create(user=user)
            if created:
                code.refresh_code()

            send_reset_password_email.apply_async((
                user.email,
                "https://www.clickbye.fun/password/reset/{}/".format(code.code),
                user.is_eng,
            ))

            return JsonResponse(data={
                "result": "success",
                "reason": "N/A"
            })

        code = ResetCode.objects.filter(code=code)
        if not request.POST.get('new_password', None):
            return JsonResponse(data={
                "result": "error",
                "reason": "New password is required"
            }, status=400)

        if code.exists():
            code = code.first()
            user = code.user

            code.refresh_code()
            user.set_password(request.POST.get('new_password'))
            user.save()
            return JsonResponse(data={
                "result": "success",
                "reason": "New password is set"
            })
        return JsonResponse(data={
            "result": "error",
            "reason": "Code does not exist"
        }, status=400)
