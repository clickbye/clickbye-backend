# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import get_user_model
from django.http import JsonResponse
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView

from api.utils.request_validators import check_param_existing, check_data_by_validator
from api.utils import is_int
from clickbyer.serializers import FlightUserSerializer
from clickbyer.utils import check_email, check_username, check_password
from clickbyer.views_mixin.welcome_campaign_mixin import WelcomeCampaignMixin


class AddNewUser(APIView, WelcomeCampaignMixin):
    @check_param_existing('email')
    @check_data_by_validator('password', check_password)
    @check_param_existing('first_name')
    @check_param_existing('last_name')
    @check_param_existing('gender')
    @check_data_by_validator('birth_year', lambda x: is_int(x) and len(x) == 4)
    @check_param_existing('home_city')
    @check_param_existing('username')
    @check_param_existing('region')
    @check_param_existing('city')
    @check_param_existing('timezone')
    @check_param_existing('locale')
    @check_param_existing('device_type')
    @check_param_existing('device_manufacturer')
    @check_param_existing('device_model')
    @check_param_existing('os_name')
    @check_param_existing('app_name')
    def post(self, request):
        is_email_valid, email_error = check_email(request.POST.get('email'))
        is_username_valid, username_error = check_username(request.POST.get('username'))

        if not is_email_valid:
            return JsonResponse(data={
                "result": "error",
                "reason": email_error
            }, status=400)

        if not is_username_valid:
            return JsonResponse(data={
                "result": "error",
                "reason": username_error
            }, status=400)

        user = get_user_model().objects.create_user(
            email=request.POST.get('email'),
            first_name=request.POST.get('first_name'),
            last_name=request.POST.get('last_name'),
            gender=request.POST.get('gender'),
            birth_year=request.POST.get('birth_year'),
            home_city=request.POST.get('home_city'),
            username=request.POST.get('username'),
            region=request.POST.get('region'),
            city=request.POST.get('city'),
            timezone=request.POST.get('timezone'),
            locale=request.POST.get('locale'),
            device_type=request.POST.get('device_type'),
            device_manufacturer=request.POST.get('device_manufacturer'),
            device_model=request.POST.get('device_model'),
            os_name=request.POST.get('os_name'),
            app_name=request.POST.get('app_name'),
            password=request.POST.get('password')
        )

        self.setup_welcome_campaign(user.pk)

        token, created = Token.objects.get_or_create(user=user)
        user = FlightUserSerializer(instance=user)
        data = {
            'Add new user': {
                'reason': u'n/a',
                'result': u'success'
            },
            'token': token.key
        }
        data.update(user.data)

        return JsonResponse(data=data, safe=False)
