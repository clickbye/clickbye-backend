# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse
from rest_framework.views import APIView

from api.utils.request_validators import check_param_existing
from clickbyer.utils import check_email


class CheckEmail(APIView):
    @check_param_existing('email', method='GET')
    def get(self, request):
        is_email_valid, error = check_email(request.GET.get('email'))
        if is_email_valid:
            return JsonResponse(data={
                'Check email': {
                    'reason': u'n/a',
                    'result': u'success'
                }
            })

        return JsonResponse(data={
            "result": "error",
            "reason": error
        }, status=400)
