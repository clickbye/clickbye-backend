# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated

from api.utils import is_int
from api.utils.request_validators import check_param_existing, check_data_by_validator
from clickbyer.utils import check_email, check_username


class UpdateUser(APIView):
    permission_classes = (IsAuthenticated,)

    @check_param_existing('email')
    @check_param_existing('first_name')
    @check_param_existing('last_name')
    @check_param_existing('gender')
    @check_data_by_validator('birth_year', lambda x: is_int(x) and len(x) == 4)
    @check_param_existing('home_city')
    @check_param_existing('username')
    def post(self, request):
        params = [
            'email',
            'first_name',
            'last_name',
            'gender',
            'birth_year',
            'home_city',
            'username'
        ]
        is_email_valid, email_error = True, 'OK'
        is_username_valid, username_error = True, 'OK'

        if request.user.email != request.POST.get('email'):
            is_email_valid, email_error = check_email(request.POST.get('email'))

        if request.user.username != request.POST.get('username'):
            is_username_valid, username_error = check_username(request.POST.get('username'))

        if not is_email_valid:
            return JsonResponse(data={
                "result": "error",
                "reason": email_error
            }, status=400)

        if not is_username_valid:
            return JsonResponse(data={
                "result": "error",
                "reason": username_error
            }, status=400)

        user = request.user
        updated_fields = []

        for param in params:
            param_value = request.POST.get(param)
            param_value = int(param_value) if param == 'birth_year' else param_value
            if param_value and getattr(user, param) != param_value:
                updated_fields.append(param)
                setattr(user, param, param_value)

        user.save()

        return JsonResponse({
            'Update new user': {
                'info updated': ", ".join(updated_fields),
                'reason': u'n/a',
                'result': u'success'
            }
        })
