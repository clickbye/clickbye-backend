from clickbye.celery_cfg import app
from core.sendgrid_helper import Sendgrid


@app.task
def send_reset_password_email(to_email, link, is_eng):
    Sendgrid().send_email_for_resetting_password(to_email, link, is_eng)
