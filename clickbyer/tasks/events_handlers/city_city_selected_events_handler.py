from collections import defaultdict
from clickbye.celery_cfg import app
from core.sendgrid_helper import Sendgrid
from api.models.sky_scanner_city import CityData
from api.utils.translated_wishes_getter import TranslatedWishesGetter

NUMBER_OF_OCCURRENCE_TO_TRIGGER = 3


def get_variables_for_event(event, index):
    print(event['event_properties']['DestinationCity'])
    destination_city = CityData.objects.filter(city_name__iexact=event['event_properties']['DestinationCity']).first()
    data = {
        '-cityname{}-'.format(index): event['event_properties']['DestinationCity'],
        #'-countryname{}-'.format(index): event['event_properties']['ArrivalDate'],
        '-maxprice{}-'.format(index): event['event_properties']['Budget'],
        '-wish{}1'.format(index): '',
        '-wish{}2'.format(index): '',
        '-wish{}3'.format(index): ''
    }
    wishes = TranslatedWishesGetter().get_wishes_list(
        destination_city.wishes,
        event['user_properties'].get('language', 'en')
    )

    for i, wish in enumerate(wishes):
        data['-wish{}1'.format(index)] = wish['image_url']
        if i == 3:
            break
    return data


def get_variables_for_events(events):
    data = {
        '-departuredate-': events[0]['event_properties'].get('DepartureDate', ''),
        '-returndate-': events[0]['event_properties'].get('ArrivalDate', ''),
    }
    for i, event in enumerate(events):
        data.update(get_variables_for_event(event, i))
    return data


def get_filtered_events(events_list):
    seen_cities = []
    result = []
    for e in events_list:
        if e['event_properties']['DestinationCity'] not in seen_cities:
            seen_cities.append(e['event_properties']['DestinationCity'])
            result.append(e)
    return result


@app.task
def city_city_selected_events_handler_task(events):
    grouped = defaultdict(list)

    for e in events:
        grouped[e['user_id']].append(e)

    for email, events_list in grouped.items():
        filtered_events = get_filtered_events(events_list)
        if len(filtered_events) >= NUMBER_OF_OCCURRENCE_TO_TRIGGER and email is not None and email != 'anonymousId':
            #if email == 'angelnessim@hotmail.com':
            events = events_list[:3]
            from pprint import pprint
            try:
                data = get_variables_for_events(events)
            except Exception as e:
                print(e)
                break
            print(email)
            pprint(events)
            pprint(data)
            #Sendgrid().send_email_for_city_city_selected_event(email, {})
            # else:
            #     print(email)
