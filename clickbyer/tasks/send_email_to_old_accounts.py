from time import sleep

from django.utils import timezone

from clickbye.celery_cfg import app
from clickbyer.models import OldAccountEmailList
from core.sendgrid_helper import Sendgrid


@app.task
def main():
    old_accounts = OldAccountEmailList.objects.filter(first_email_sent_date=None)[:4000]
    for index, oa in enumerate(old_accounts):
        Sendgrid().send_email_for_old_emails_activity_campaign(
            oa.email,
            oa.name.capitalize(),
            oa.is_eng
        )
        oa.first_email_sent_date = timezone.now()
        oa.save()
        if not index % 20:
            sleep(60)
