import requests
import zipfile
import os
import gzip
import json
import tempfile
from collections import defaultdict
from datetime import timedelta, datetime

from clickbye.celery_cfg import app

from clickbyer.tasks.events_handlers.city_city_selected_events_handler import city_city_selected_events_handler_task


EVENTS_TYPES = {
   'City_City_Selected': city_city_selected_events_handler_task,
}


class EventsGatherer:
    API_KEY = "8da231bcde2c0c7323ba58b376d9a1f6"
    API_SECRET = "e72c4ce0e905377e22d1338c767ea04e"
    URL_TEMPLATE = 'https://amplitude.com/api/2/export?start={}&end={}'
    PATH = tempfile.gettempdir()
    EXTRACTED_PATH = os.path.join(PATH, 'events_extracted')
    ZIP_NAME = os.path.join(PATH, 'events.zip')

    def __init__(self):
        today = datetime.now()
        yesterday = today - timedelta(days=1)
        start = "{}{}{}T10".format(yesterday.year, yesterday.strftime('%m'), yesterday.strftime('%d'))
        end = "{}{}{}T19".format(today.year, today.strftime('%m'), today.strftime('%d'))
        self.url = self.URL_TEMPLATE.format(start, end)
        self._result_events = defaultdict(list)

    @property
    def result_events(self):
        if not self._result_events:
            self.gather_events()
        return self._result_events

    def gather_events(self):
        self._result_events = defaultdict(list)
        self._save_zip_file()
        self._extract_files_from_zip()
        self._gather_events()

    def _save_zip_file(self):
        response = requests.get(self.url, auth=(self.API_KEY, self.API_SECRET))
        with open(self.ZIP_NAME, 'w+b') as out_file:
            out_file.write(response.content)

        with open(self.ZIP_NAME, 'r+b') as f:
            content = f.read()
            pos = content.find(str.encode('\x50\x4b\x05\x06'))
            if pos > 0:
                f.seek(pos + 22)
                f.truncate()
                f.close()

    def _extract_files_from_zip(self):
        with zipfile.ZipFile(self.ZIP_NAME) as zf:
            zf.extractall(path=self.EXTRACTED_PATH)

    def _gather_events(self):
        for root, dirs, files in os.walk(self.EXTRACTED_PATH):
            for f in files:
                if f.endswith('.gz'):
                    self._gather_events_from_file(os.path.join(root, f))

    def _gather_events_from_file(self, file_path):
        with gzip.open(file_path, 'rb') as f:
            for line in f.readlines():
                d = json.loads(line.strip())
                self._result_events[d['event_type']].append(d)


@app.task
def gather_and_dispatch_events():
    result_events = EventsGatherer().result_events

    for event_type, task in EVENTS_TYPES.items():
        # from pprint import pprint
        # for r in result_events[event_type]:
        #     if r['user_id'] and r['user_id'].startswith('clickbye'):
        #         pprint(r)
        #         return
        #task.apply_async((result_events[event_type],))
        task(result_events[event_type])


if __name__ == "__main__":
    gather_and_dispatch_events()
    #from clickbyer.tasks.events_gatherer import gather_and_dispatch_events;gather_and_dispatch_events()