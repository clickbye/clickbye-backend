from clickbye.celery_cfg import app
from clickbyer.models import FlightUser
from core.sendgrid_helper import Sendgrid


@app.task(name='welcome_email_1')
def send_first_welcome_email(clickbyer_pk):
    clickbyer = FlightUser.objects.get(pk=clickbyer_pk)
    Sendgrid().send_email_for_welcome_campaign(clickbyer.email, clickbyer.first_name, clickbyer.is_eng)


@app.task(name='welcome_email_3')
def send_3_days_welcome_email(clickbyer_pk):
    clickbyer = FlightUser.objects.get(pk=clickbyer_pk)
    Sendgrid().send_email_for_welcome_campaign_after_3_days(clickbyer.email, clickbyer.first_name, clickbyer.is_eng)


@app.task(name='welcome_email_7')
def send_7_days_welcome_email(clickbyer_pk):
    clickbyer = FlightUser.objects.get(pk=clickbyer_pk)
    Sendgrid().send_email_for_welcome_campaign_after_7_days(clickbyer.email, clickbyer.first_name, clickbyer.is_eng)
