from django.conf.urls import url

from clickbyer.views.check_email import CheckEmail
from clickbyer.views.check_username import CheckUsername
from clickbyer.views.add_new_user import AddNewUser
from clickbyer.views.token_login import TokenLogin
from clickbyer.views.update_user import UpdateUser
from clickbyer.views.connect_user_social_network import ConnectUserSocialNetwork
from clickbyer.views.update_user_password import UpdateUserPassword
from clickbyer.views.password_reset import PasswordReset


app_name = 'clickbyer'
urlpatterns = [
    url(r'^login/', TokenLogin.as_view(), name='token_login'),
    url(r'^check_email/$', CheckEmail.as_view(), name='check_email'),
    url(r'^check_username/$', CheckUsername.as_view(), name='check_username'),
    url(r'^add_new/$', AddNewUser.as_view(), name='add_new_user'),
    url(r'^update_profile/$', UpdateUser.as_view(), name='update_user'),
    url(r'^connect_social_network/$', ConnectUserSocialNetwork.as_view(), name='connect_user_social'),

    url(r'^password/update/$', UpdateUserPassword.as_view(), name='update_user_password'),
    url(r'^password/reset/$', PasswordReset.as_view(), name='password-reset'),
    url(r'^password/reset/(?P<code>[^/]+)/$', PasswordReset.as_view(), name='password-reset'),
]
