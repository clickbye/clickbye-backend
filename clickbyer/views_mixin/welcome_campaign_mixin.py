from datetime import timedelta

from django.utils import timezone
from clickbyer.tasks.welcome_campaign import send_3_days_welcome_email, send_7_days_welcome_email,\
    send_first_welcome_email


class WelcomeCampaignMixin:
    @staticmethod
    def setup_welcome_campaign(user_pk):
        days_3_re_time = timezone.now() + timedelta(days=3)
        days_7_re_time = timezone.now() + timedelta(days=7)

        send_first_welcome_email.apply_async((user_pk,))
        send_3_days_welcome_email.apply_async((user_pk,), eta=days_3_re_time)
        send_7_days_welcome_email.apply_async((user_pk,), eta=days_7_re_time)
