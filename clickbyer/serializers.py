from rest_framework import serializers

from .models import FlightUser


class FlightUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = FlightUser
        fields = [
            'email',
            'username',
            'first_name',
            'last_name',
            'gender',
            'birth_year',
            'home_city',
            'id'
        ]
