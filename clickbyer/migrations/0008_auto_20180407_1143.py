# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-04-07 11:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('clickbyer', '0007_auto_20180405_1928'),
    ]

    operations = [
        migrations.AddField(
            model_name='oldaccountemaillist',
            name='is_eng',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='oldaccountemaillist',
            name='name',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
    ]
